/*
  Blank Simple Project.c
  http://learn.parallax.com/propeller-c-tutorials 
*/
#include "simpletools.h"                      // Include simple too                      // Include adcDCpropab
#include "adcDCpropab.h"
#include "dht22.h"

int main()                                    // Main function
{
  // Add startup code here.
  
  adc_init(21, 20, 19, 18);                   // CS=21, SCL=20, DO=19, DI=18
  float v1;                              // tempurature sensor voltage
  float v2;                              // Moisture sensor 1
  float v3;                              // Moisture sensore 2

  while(1)
  {
    v2 = adc_volts(2);                        // A/D converter for moisture sensors
    v3 = adc_volts(1);  
    dht22_read(9);                            // uses dht22 library to get reading from dht22 temp sensor
    pause(500);                               // dht22_read requires a 500ms delay between readings unless another function is used
    v1 = dht22_getTemp(CELSIUS)/100;          // dht22 library reads temp. since a dht11 temp sensor was used this reading is funky but can be used if calibrated.
    putChar(HOME);                            // Cursor -> top-left "home"
    print("A/D2 = %f V%c\n", v2, CLREOL);     // Display voltages of moisture sensor 1
    print("A/D0 = %f V%c\n", v3, CLREOL);     // Display voltages of moisture sensor 2
    print("%s%03.1f%s", "The Temperature is ", v1, "Degrees F", CLREOL); // Display tempurature

    pause(100);                               // Wait 1/10 s
   if (v2 < 1)                                // doesnt need water
    {
      low(0);
      low(14);  
      pause(200);
      high(14);   
    }     
    if (v2 > 2 && v1 > 50)                    // does need water
    {
      high(0);
      low(15);
      pause(200);
      high(15);
    }
    if (v3 < 1)                               // doesnt need water
    {
      low(0);
      low(12);  
      pause(200);
      high(12);   
    }     
    if (v3 > 2 && v1 > 50)                // does need water
    {
     high(0);
     low(13);
     pause(200);
     high(13);
    }
    else
    {
     low(0);
     high(12);
     high(13);
     high(14);
     high(15);
    }
}
